const Builder = @import("std").build.Builder;

const libraries = [_][]const u8{
    "c",
    "m",
    "dill",
    "nettle",
    "jansson",
    "curl",
};

const source_files = [_][]const u8{
    "src/yasuna.c", "src/rest.c",    "src/events.c",
    "src/log.c",    "src/gateway.c",
};

const c_args = [_][]const u8{
    "-Wunreachable-code",
    "-Wall",
    "-Wpedantic",
    "-fPIC",
};

const include_dirs = [_][]const u8{
    "src",
    "src/include",
    "/usr/local/include",
    "/usr/include",
};

pub fn build(b: *Builder) void {
    const mode = b.standardReleaseOptions();

    // export both dynamic and static libraries.
    const dynlib = b.addSharedLibrary("yasuna", null, b.version(0, 0, 1));
    const stlib = b.addStaticLibrary("yasuna", null);

    const exe = b.addExecutable("pong", null);
    exe.linkSystemLibrary("yasuna");
    exe.addCSourceFile("examples/pong.c", &c_args);

    dynlib.setBuildMode(mode);
    stlib.setBuildMode(mode);

    exe.setBuildMode(mode);
    exe.linkLibrary(dynlib);

    for (include_dirs) |include_dir| {
        dynlib.addSystemIncludeDir(include_dir);
        stlib.addSystemIncludeDir(include_dir);
        exe.addSystemIncludeDir(include_dir);
    }

    const lib_paths = [_][]const u8{"/usr/local/lib"};
    for (lib_paths) |path| {
        dynlib.addLibPath(path);
        stlib.addLibPath(path);
        exe.addLibPath(path);
    }

    for (libraries) |sys_lib| {
        dynlib.linkSystemLibrary(sys_lib);
        stlib.linkSystemLibrary(sys_lib);
        exe.linkSystemLibrary(sys_lib);
    }

    for (source_files) |source| {
        dynlib.addCSourceFile(source, &c_args);
        stlib.addCSourceFile(source, &c_args);
    }

    const zig_sources = [_][]const u8{
        "src/guild_store.zig",
        "src/client.zig",
        "src/repr.zig",
        "src/dt_from_json.zig",
        "src/client_internal.zig",
    };

    const obj_name = [_][]const u8{
        "guild_store",
        "client",
        "repr",
        "dt_from_json",
        "client_internal",
    };

    for (zig_sources) |source, idx| {
        const obj = b.addObject(obj_name[idx], source);
        obj.linkSystemLibrary("c");

        for (include_dirs) |dir| obj.addSystemIncludeDir(dir);
        for (lib_paths) |path| obj.addLibPath(path);

        dynlib.addObject(obj);
        stlib.addObject(obj);
    }

    // 'zig build run' will run the pong example

    const run_cmd = exe.run();
    const run_step = b.step("run-pong", "Run the pong example");
    run_step.dependOn(&run_cmd.step);

    b.default_step.dependOn(&dynlib.step);
    dynlib.install();
    stlib.install();
    //b.installArtifact(dynlib);
    //b.installArtifact(stlib);
}
