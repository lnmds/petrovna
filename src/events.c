#include <stdlib.h>
#include <string.h>
#include "libyasuna-private.h"

// yasuna event handling happens with a linked list
typedef struct ev_handlers {
    yn_event_handler_t callback;
    char handler_name[YN_EVT_HANDLER_NAME_MAX];

    struct ev_handlers* next;
} yn_ev_handlers_t;

// this holds an array from the event to a linked list
// of event handlers
yn_ev_handlers_t* listeners[YN_EVT_MAX_EVT];

// only call this function once.
void yn_event_init()
{
    for(int i = 0; i < YN_EVT_MAX_EVT; i++)
    {
        listeners[i] = NULL;
    }
}

// create a new handler, returns its pointer
yn_ev_handlers_t* yn_event_new_handler(
        yn_event_handler_t handler, char* name)
{
    // allocate handler
    yn_ev_handlers_t *new_handler = malloc(sizeof(yn_ev_handlers_t));

    if(new_handler == NULL)
    {
        log_fatal("failed to allocate for new handler");
        return NULL;
    }

    new_handler->next = NULL;
    new_handler->callback = handler;

    // doing simple assign doesn't work due to type mismatch
    memcpy(&(new_handler->handler_name), name, YN_EVT_HANDLER_NAME_MAX);
    return new_handler;
}

// add a given handler to a given event
void yn_handler_add(yn_event event, yn_event_handler_t handler,
        char* name)
{
    yn_ev_handlers_t *current = listeners[event];
    log_trace("add handler for %d: '%s'", event, name);

    if(current == NULL)
    {
        log_trace("current is null, reassigning");
        current = yn_event_new_handler(handler, name);

        if(current == NULL)
        {
            log_fatal("failed to assign first node");
            return;
        }

        // NOTE: make the new head node available since just assigning to
        // current won't work
        listeners[event] = current;
        return;
    }

    // walk towards the final node in the list
    while(current->next != NULL)
        current = current->next;

    // allocate handler
    yn_ev_handlers_t *new_handler = yn_event_new_handler(handler, name);

    if(new_handler == NULL)
    {
        log_fatal("failed to allocate for new handler");
        return;
    }

    current->next = new_handler;
}

// inspect handlers for a given event
void yn_event_inspect(yn_event event)
{
    log_trace("event int: %d", event);

    yn_ev_handlers_t* current = listeners[event];
    while(current != NULL)
    {
        log_trace("event %d: handler %s",
                event, current->handler_name);
        current = current->next;
    }
}

// emit an event
void yn_event_emit(yn_client* client, yn_event event, void* user_data)
{
    yn_ev_handlers_t* handlers = listeners[event];
    log_trace("emitting event %d, handlers = %p", event, handlers);

    for(; handlers != NULL; handlers = handlers->next)
    {
        log_trace("emitting event %d: handler '%s'",
                event, handlers->handler_name);
        handlers->callback(client, user_data);
    }

    log_trace("done");
}

// remove an event handler by its name
void yn_event_remove(yn_event event, char *name)
{
    yn_ev_handlers_t *handlers = listeners[event];
    for(; handlers != NULL; handlers = handlers->next)
    {
        // this checks the next handler in the given event
        // linked list and if it matches the name, it will overwrite
        // the current next pointer to the next's next pointer.
        if(handlers->next == NULL)
            return;

        yn_ev_handlers_t* next = handlers->next;

        if(STREQ(next->handler_name, name))
        {
            handlers->next = next->next;
        }

        free(next);
    }
}
