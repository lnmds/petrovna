const std = @import("std");
usingnamespace @import("helpers.zig");

export fn yn_message_repr(
    message: *c.yn_message_t,
    out: [*c]u8,
    out_len: usize,
) usize {
    const result = std.fmt.bufPrint(
        out[0..out_len],
        "yn_message<{},chan_id={},author_id={},type={}>",
        .{
            message.id,
            message.channel_id,
            message.author_id,
            message.@"type",
        },
    ) catch {
        std.debug.warn("failed to repr yn_user\n", .{});
        return 0;
    };

    return result.len;
}

export fn yn_user_repr(
    user: *c.yn_user_t,
    out: [*c]u8,
    out_len: usize,
) usize {
    const result = std.fmt.bufPrint(
        out[0..out_len],
        "yn_user<{},'{}#{}',avatar='{}',bot={}>",
        .{
            sliceify(user.id),
            sliceify(user.username),
            sliceify(user.discriminator),
            sliceify(user.avatar),
            user.bot,
        },
    ) catch {
        std.debug.warn("failed to repr yn_user\n", .{});
        return 0;
    };

    return result.len;
}
