const std = @import("std");

pub const c = @cImport({
    @cInclude("log.h");
    @cInclude("libyasuna-private.h");
});

pub const yn_client = c.yn_client;
pub const yn_message_t = c.yn_message_t;
pub const yn_user_t = c.yn_user_t;
pub const yn_guild_t = c.yn_guild_t;
pub const yn_unavailable_guild_t = c.yn_unavailable_guild_t;
pub const yn_snowflake_t = c.yn_snowflake_t;
pub const yn_client_internal = c.yn_client_internal;

pub fn ynPriv(client: *yn_client) *c.yn_client_internal {
    var priv = @ptrCast([*c]yn_client_internal, @alignCast(@alignOf(yn_client_internal), client.priv));
    return priv;
}

pub fn json_get_str(data: *c.json_t, field: [*]const u8) [*]const u8 {
    return c.json_string_value(c.json_object_get(data, field));
}

pub fn json_get_bool(data: *c.json_t, field: [*]const u8) bool {
    // hack since json_boolean_value is json_is_true and that's a macro that
    // couldn't be translated:
    //  #define json_is_true ((json) && json_typeof(json) == JSON_TRUE)
    var val_opt = c.json_object_get(data, field);
    if (val_opt == null) return false;

    // easy recast
    var val: *c.json_t = val_opt.?;
    return val.@"type" == c.json_type.JSON_TRUE;
}

pub fn json_get_int(data: *c.json_t, field: [*]const u8) c_longlong {
    return c.json_integer_value(c.json_object_get(data, field));
}

pub fn sliceify(non_slice: [*:0]const u8) []const u8 {
    return std.mem.span(non_slice);
}

/// Compare two c strings.
pub fn zig_streq(str1: [*:0]const u8, str2: [*:0]const u8) bool {
    return std.cstr.cmp(str1, str2) == 0;
}
