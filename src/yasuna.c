#include <stdio.h>
#include <stdlib.h>

#include "libyasuna-private.h"

// main file that puts things together

yn_client *yn_client_create_discord(char *token)
{
    return yn_client_make("https://discordapp.com/api/v6", token);
}

int yn_main_loop(yn_client *client)
{
    // to properly start main loop we need to fetch a gateway url,
    // start our websockets, and *then* enter a main loop for gateway
    // messages.

    const char *gw_path = yn_rest_get_gateway(client->api_base);
    if(gw_path == NULL)
    {
        perror("can't get gateway url");
        return 1;
    }

    struct ipaddr addr;
    int status = yn_gw_find(&addr, gw_path, 443);
    if(status == -1)
    {
        perror("yn_gw_find");
        return 1;
    }

    int sockfd = yn_gw_connect(&addr, gw_path);
    if(sockfd == -1)
    {
        perror("Failed to connect to the gateway");
        return 1;
    }

    yn_client_internal *priv = client->priv;
    priv->sockfd = sockfd;

    // start main loop for incoming gateway messages
    priv->ws_loop_handle = go(ws_loop(client));

    // we now need to start our own loop to keep the other
    // coroutines running well
    while(priv->run_flag)
        msleep(now() + 1000);

    // this loop only stops on an error or run_flag=false, which
    // can then be set by a sigint handler or smth else

    // when exiting the loop, we must check for errors written
    // in the client
    if(client->ws_err->set)
    {
        fprintf(stderr, "error on ws_loop. code: %d, reason: '%s'\n",
                client->ws_err->code, client->ws_err->reason);
    }

    // close client
    status = yn_client_close(client);
    if(status == -1)
    {
        perror("yn_client_close");
        return 1;
    }

    // we exited the main loop, so there isn't much to do other than
    // closing and saying bye bye
    return 0;
}
