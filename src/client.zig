const std = @import("std");
usingnamespace @import("helpers.zig");

pub export fn yn_client_make(api_base: [*c]u8, token: [*c]u8) [*c]yn_client {
    c.yn_event_init();

    var client = std.heap.c_allocator.create(yn_client) catch |err| {
        std.debug.warn("failed to allocate client\n", .{});
        return null;
    };

    client.api_base = api_base;
    client.token = token;
    client.user = std.heap.c_allocator.create(c.yn_user_t) catch |err| {
        std.debug.warn("failed to allocate user struct\n", .{});
        return null;
    };

    var priv = std.heap.c_allocator.create(yn_client_internal) catch |err| {
        std.debug.warn("failed to allocate priv struct\n", .{});
        return null;
    };

    priv.sockfd = -1;
    priv.sequence = 0;
    priv.hb_handler = -1;
    priv.ws_loop_handle = -1;
    priv.@"resume" = false;
    priv.run_flag = true;
    var sess_id = std.heap.c_allocator.alloc(u8, c.YN_CLIENT_SESSID_MAX) catch |err| {
        std.debug.warn("failed to allocate user struct\n", .{});
        return null;
    };

    priv.session_id = sess_id.ptr;

    //priv.session_id = @ptrCast([*c]const u8, sess_id.ptr);
    priv.guild_store_used = @as(usize, 0);

    client.priv = @ptrCast(?*c_void, priv);

    client.ws_err = std.heap.c_allocator.create(c.yn_wserr_t) catch |err| {
        std.debug.warn("failed to allocate ws err struct\n", .{});
        return null;
    };

    var ws_err = client.ws_err.?[0];
    ws_err.set = false;
    ws_err.code = 0;

    var default_reason = std.cstr.addNullByte(std.heap.c_allocator, "") catch |err| {
        std.debug.warn("failed to allocate default reason\n", .{});
        return null;
    };
    ws_err.reason = default_reason.ptr;

    return client;
}

export fn free_priv(priv: [*c]yn_client_internal) void {
    std.heap.c_allocator.destroy(priv);
}

pub export fn yn_client_close(client_ptr: [*c]yn_client) c_int {
    var status: c_int = undefined;
    if (client_ptr == null) {
        return 1;
    }

    var client: *yn_client = client_ptr.?;

    // TODO logging on stdlib
    std.debug.warn("destroying client\n", .{});

    var priv = ynPriv(client);

    if (!priv.run_flag) return 0;
    priv.run_flag = false;

    std.debug.warn("trying to close heartbeat\n", .{});

    status = c.dill_hclose(priv.hb_handler);
    if (status != 0) {
        std.debug.warn("hb_handler:close\n", .{});
        return 1;
    }

    std.debug.warn("trying to close ws loop\n", .{});
    status = c.dill_hclose(priv.ws_loop_handle);
    if (status != 0) {
        std.debug.warn("ws_loop:close\n", .{});
        return 1;
    }

    status = c.yn_gw_close(priv.sockfd);
    if (status != 0) {
        std.debug.warn("yn_gw_close\n", .{});
        return 1;
    }

    std.heap.c_allocator.destroy(client.token);
    std.heap.c_allocator.destroy(client.guilds);
    std.heap.c_allocator.destroy(client.unavailable_guilds);
    free_priv(priv);

    return 0;
}
