const std = @import("std");
usingnamespace @import("helpers.zig");

export fn yn_guild_store_get(
    client: *yn_client,
    guild_id: yn_snowflake_t,
) ?*yn_guild_t {
    var priv = ynPriv(client);
    var i: usize = 0;

    while (i < client.guild_size) : (i += 1) {
        var guild_ptr: [*c]yn_guild_t = &client.guilds[i];
        var guild: *yn_guild_t = guild_ptr.?;

        if (!guild._init) continue;
        if (zig_streq(guild.id, guild_id)) return guild;
    }

    return null;
}

export fn yn_guild_store_insert(
    client_ptr: [*c]yn_client,
    insert_guild_ptr: [*c]yn_guild_t,
) c_int {
    var client: *yn_client = client_ptr.?;
    var insert_guild: *yn_guild_t = insert_guild_ptr.?;

    var priv = ynPriv(client);
    var i: usize = 0;

    while (i < priv.guild_store_used) : (i +%= 1) {
        var guild_ptr: [*c]yn_guild_t = &client.guilds[i];
        var guild: *yn_guild_t = guild_ptr.?;

        if (!guild._init) continue;
        if (zig_streq(guild.id, insert_guild.id)) return 1;
    }

    client.guilds[i +% @as(c_ulong, 1)] = insert_guild.*;
    return 0;
}

pub export fn yn_guild_store_del(arg0: [*c]c.yn_client, arg1: c.yn_snowflake_t) c_int {
    return 0;
}
