#include <sys/types.h>
#include <sys/socket.h>
#include <netdb.h>
#include <arpa/inet.h>
#include <string.h>
#include <stdlib.h>
#include <unistd.h>
#include <stdio.h>
#include <errno.h>

#include <nettle/base64.h>
#include <libdill.h>

#include "libyasuna-private.h"
#include "log.h"

#define YN_INT_QUICK_EVT 20

// helpers for event dispatching, maps an event to...
const char* events[YN_INT_QUICK_EVT] = {
    "READY", "GUILD_CREATE", "MESSAGE_CREATE",
};

// holds functions that convert from json_t to a void pointer
// that *should* point to one of yasuna's main types (yn_guild,
// yn_message, etc)
#define CONVERTER(fun_ptr) (void* (*) (json_t*))(&fun_ptr)

void* (*quick_evt_user_data[YN_INT_QUICK_EVT])(json_t*) = {
    NULL, NULL, CONVERTER(yn_message_from_json)
};

// internal client handlers not connected to main events
int (*default_client_handlers[YN_INT_QUICK_EVT])(yn_client*, void*) = {
    yn_intr_client_ready, yn_intr_guild_create, NULL
};

// mapping payload.t to yn_event
yn_event quick_evt_map[YN_INT_QUICK_EVT] = {
    YN_EVT_READY, YN_EVT_MAX_EVT, YN_EVT_MESSAGE_CREATE,
};

// handler for op 0 dispatch
int process_dispatch(yn_client *client, json_t *payload)
{
    const char *evt;
    json_t *evt_obj, *event_data;

    // specific to event dispatching
    // user_data holds specific user data such as e.g
    // the message in the given MESSAGE_CREATE event. we need
    // to type it as somewhere, and C typing doesn't allow
    // unions (two types together, not the union type gdi).
    void* user_data;

    // pointers to the json_t*->void* converter and
    // the internal client handler for the event, that is
    // separate from the main event dispatch infrastructure
    void* (*converter)(json_t*);
    int (*internal_handler)(yn_client*, void*);

    // holds the yn_event related to the payload.t of the event
    yn_event event_to_emit;

    evt_obj = json_object_get(payload, "t");
    evt = json_string_value(evt_obj);

    if(evt == NULL)
    {
        log_fatal("op 0 packet without event");
        return 1;
    }

    log_trace("Event: '%s'", evt);

    event_data = json_object_get(payload, "d");

    // main event handling happens here
    for(int i = 0; i < YN_INT_QUICK_EVT; i++)
    {
        // if we find a matching pair (payload.t, events[i]), then we
        // go on to more advanced event dispatching
        if(STREQ(evt, events[i]))
        {
            // first thing is converting the given payload.d (event data)
            // which is a json_t pointer to a void pointer so that internal
            // event handler and other custom handlers can cast it to its
            // supposed value
            converter = quick_evt_user_data[i];

            // if no converter is declared for the event, by default
            // we assign user_data to the raw json_t*.
            if(converter != NULL)
                user_data = converter(event_data);
            else
                user_data = event_data;

            internal_handler = default_client_handlers[i];

            // TODO: check value returned by internal_handler
            if(internal_handler != NULL)
                internal_handler(client, user_data);

            event_to_emit = quick_evt_map[i];

            // TODO: check if event_to_emit is NULL
            yn_event_emit(client, event_to_emit, user_data);
        }
    }

    json_decref(evt_obj);
    return 0;
}

coroutine void heartbeat_handler(yn_client *client, int hb_interval)
{
    log_trace("heartbeat handler start!");

    json_t *root_obj = json_object();
    json_object_set(root_obj, "op", json_integer(OP_HEARTBEAT));

    yn_client_internal *priv = client->priv;

    while(priv->run_flag)
    {
        json_object_set(root_obj, "d", json_integer(priv->sequence));
        char *data = JSON_DUMPS(root_obj);

        log_trace("Sending heartbeat at sequence %d", priv->sequence);

        // send the heartbeat packet
        int status = ws_send(priv->sockfd, WS_BINARY,
            data, strlen(data), -1);

        // we don't need it anymore, so..
        // TODO: check if this works
        // free(data);

        if(status == -1)
        {
            log_fatal("Failed to send heartbeat packet");
            perror("ws_send");
            break;
        }

        log_trace("waiting %d milliseconds", hb_interval);
        msleep(now() + hb_interval);
    }

    json_decref(root_obj);
}

coroutine void gw_identify(yn_client *client)
{
    log_info("Identifying with the gateway");

    json_t *root_obj, *identify_obj, *connprop_obj;

    root_obj = json_object();
    json_object_set(root_obj, "op", json_integer(OP_IDENTIFY));

    identify_obj = json_object();
    json_object_set(identify_obj, "token", json_string(client->token));
    json_object_set(identify_obj, "compress", json_false());

    connprop_obj = json_object();
    json_object_set(connprop_obj, "$os", json_string("Linux"));
    json_object_set(connprop_obj, "$device", json_string("yasuna"));
    json_object_set(connprop_obj, "$browser",
            json_string("yasuna (https://gitlab.com/luna/yasuna)"));

    json_object_set(identify_obj, "properties", connprop_obj);
    json_object_set(root_obj, "d", identify_obj);

    // serialize and send!
    char *data = JSON_DUMPS(root_obj);

    log_trace("sending packet: %s", data);
    yn_client_internal *priv = client->priv;
    int status = ws_send(priv->sockfd, WS_BINARY, data, strlen(data), -1);

    if(status == -1)
    {
        log_fatal("Failed to send packet");
        perror("ws_send");
        return;
    }

    priv->resume = false;
    json_decref(root_obj);
}

coroutine void gw_resume(yn_client *client)
{
    log_info("Resuming with the gateway");
}

int process_message(yn_client *client, char *message)
{
    int status, payload_op, sequence, hb_interval;
    size_t message_len;
    bool resumable_session;
    json_t *root, *hello_data;
    json_error_t error;

    // allocate and parse the given message
    message_len = strlen(message);

    if(message_len <= 1024)
        log_trace("incoming ws message: '%s'", message);
    else
        log_trace("incoming ws message (too big)", message);

    root = json_loads(message, JSON_DECODE_ANY, &error);

    if(root == NULL)
    {
        log_fatal("failed to parse json from websocket: '%s'", error.text);
        return 1;
    }

    payload_op = json_number_value(json_object_get(root, "op"));

    log_trace("op of the packet: %d", payload_op);

    yn_client_internal *priv = client->priv;

    switch(payload_op)
    {
        case OP_DISPATCH:
            sequence = json_number_value(json_object_get(root, "s"));
            log_trace("seq update: %d -> %d", priv->sequence, sequence);
            priv->sequence = sequence;

            status = process_dispatch(client, root);
            if (status == -1)
            {
                log_error("Error on dispatch");
                return -1;
            }
            goto success;
        case OP_HEARTBEAT_ACK:
            log_info("got a heartbeat ack!");
            goto success;
        case OP_HELLO:
            log_info("got a hello packet!");
            hello_data = json_object_get(root, "d");
            hb_interval = json_number_value(
                    json_object_get(hello_data, "heartbeat_interval"));

            priv->hb_handler = go(heartbeat_handler(client, hb_interval));

            if(priv->resume)
            {
                go(gw_resume(client));
            } else {
                go(gw_identify(client));
            }
            goto success;
        case OP_INVALID_SESSION:
            log_info("session is invalid");
            resumable_session = json_boolean_value(
                    json_object_get(root, "d"));

            if(resumable_session)
            {
                goto err;
            } else {
                log_trace("reidentifying");
                go(gw_identify(client));
            }

            goto success;
        default:
            log_error("Unknown op code: %d", payload_op);
            goto err;
    }
    

err:
    //json_decref(root);
    log_fatal("error");
    return 1;
success:
    //json_decref(root);
    return 0;
}

void process_error(yn_client *client)
{
    if(!client->ws_err->set)
    {
        return;
    }

    log_info("Trying to recover from error code %d", client->ws_err->code);

    yn_client_internal *priv = client->priv;

    switch(client->ws_err->code)
    {
        // Unrecoverable
        case YN_WSERR_DECODE_ERROR:
            log_error("Decode error: '%s'", client->ws_err->reason);
            break;
        case YN_WSERR_INVALID_SHARD:
            log_error("Invalid shard");
            break;
        case YN_WSERR_SHARDING_REQUIRED:
            log_error("Sharding required!");
            break;
        case YN_WSERR_AUTH_FAILED:
            log_error("Authentication failure.");
            break;
        case YN_WSERR_ALREADY_AUTH:
            log_error("Already authenticated.");
            break;

        // recoverable, and we should try resuming first
        case YN_WSERR_INVALID_SEQ:
        case YN_WSERR_SESSION_TIMEOUT:
        case YN_WSERR_RATE_LIMITED:
            priv->resume = true;
            ws_loop(client);
            break;

        // Default is retry
        default:
            priv->resume = false;
            ws_loop(client);
            break;
    }
}

void set_client_ws_err(yn_client *client, int code, char *reason)
{
    yn_wserr_t *ws_err = client->ws_err;

    ws_err->set = true;
    ws_err->code = code;
    ws_err->reason = reason;
}

coroutine void ws_loop(yn_client *client)
{
    int flags, status, ws_status_num;

    // god fuck discord with their big messages
    char *buf = malloc(YN_GATEWAY_MSG_MAX_SIZE);

    if(buf == NULL)
    {
        log_fatal("failed to allocate memory for gateway messages");
        return;
    }

    char term_msg[256];

    yn_client_internal *priv = client->priv;

    while(priv->run_flag)
    {
        memset(buf, 0, YN_GATEWAY_MSG_MAX_SIZE);

        int sockfd = priv->sockfd;

        // try to fetch a message, put it in our buffer
        ssize_t sz = ws_recv(sockfd, &flags, buf,
                YN_GATEWAY_MSG_MAX_SIZE, -1);

        if(sz == -1)
        {
            perror("ws_recv");

            // *try* to fetch error via ws_status. also the websocket
            // error reason under term_msg
            ssize_t szs = ws_status(sockfd, &ws_status_num,
                    &term_msg, sizeof(term_msg));
            if(szs == -1)
            {
                perror("ws_status");

                set_client_ws_err(client, -1,
                    "failed to fetch websocket status");

                break;
            }

            set_client_ws_err(client, ws_status_num, term_msg);

            // Some errors from discord are recoverable
            process_error(client);

            log_error("status: %d, term: %s", ws_status_num, term_msg);
            perror("ws_recv");
            break;
        }

        status = process_message(client, buf);
        if(status != 0)
        {
            perror("process_messsage");
            break;
        }
    }

    log_trace("main client loop end");
}

int run_handshake(int sockfd, const char *host)
{
    // Discord only allows TLS connections.
    log_trace("tls attach: %d", sockfd);
    sockfd = tls_attach_client(sockfd, -1);
    log_trace("tls attach end: %d", sockfd);

    if(sockfd == -1)
    {
        log_fatal("Failed to attach a TLS connection");
        perror("tls_attach_client");
        return -1;
    }

    // attach websocket on top
    log_trace("ws attach: %d", sockfd);
    sockfd = ws_attach_client(sockfd, WS_BINARY, "/", host, -1);
    log_trace("ws attach end: %d", sockfd);

    if(sockfd == -1)
    {
        log_fatal("Failed to attach Websocket");
        perror("ws_attach_client");
        return -1;
    }

    return sockfd;
}

int yn_gw_connect(struct ipaddr *gw_addr, const char* gw_url)
{
    char buf[IPADDR_MAXSTRLEN];
    int sockfd;

    // only for this trace
    ipaddr_str(gw_addr, buf);
    log_trace("IP Address: %s", buf);

    sockfd = tcp_connect(gw_addr, -1);

    if(sockfd == -1)
    {
        log_fatal("Failed to establish tcp.");
        perror("tcp_connect");
        return -1;
    }

    return run_handshake(sockfd, gw_url);
}

int yn_gw_close(int sockfd)
{
    log_trace("detach ws, sockfd = %d", sockfd);
    //sockfd = ws_detach(sockfd, 1000, NULL, 0, -1);
    sockfd = ws_detach(sockfd, 1000, "", 0, -1);
    log_trace("detach ws finish, sockfd = %d", sockfd);

    if (sockfd == -1)
    {
        log_fatal("Failed to detach websocket.");
        perror("ws_detach");
        return -1;
    }

    log_trace("detach tls, sockfd = %d", sockfd);
    sockfd = tls_detach(sockfd, -1);
    log_trace("detach tls finish, sockfd = %d", sockfd);

    if (sockfd == -1)
    {
        log_fatal("Failed to detach TLS.");
        perror("tls_detach");
        return -1;
    }

    log_trace("close tcp, sockfd = %d", sockfd);
    tcp_close(sockfd, -1);
    log_trace("close tcp, sockfd = %d", sockfd);

    if (sockfd == -1)
    {
        log_fatal("Failed to close TCP connection.");
        perror("tcp_close");
        return -1;
    }

    return 0;
}

int yn_gw_find(struct ipaddr *res, const char* path, const int port)
{
    struct addrinfo hints;
    struct addrinfo *result;

    // set hints to find
    memset(&hints, 0, sizeof(struct addrinfo));
    hints.ai_family = AF_INET;
    hints.ai_socktype = SOCK_STREAM;
    hints.ai_protocol = 0;
    hints.ai_flags = 0;

    char port_str[5];
    sprintf(port_str, "%d", port);

    log_trace("Calling getaddrinfo, path='%s', port=%d", path, port);

    /*
     * Disclaimer here:
     *  The first solution of this was using getaddrinfo(),
     *  but it turned out to be a clunky mess using all the
     *  BSD socket API and all.
     *
     *  After that, I discovered that libdill has their own
     *  socket functions, and started changing this function
     *  to using ipaddr_remote()
     *
     *  It wasn't working, for some reason, after some searching
     *  I found that ipaddr_remote() uses a 3rd party
     *  DNS library, which wasn't tested, and that caused
     *  the issues I was seeing with the libdill version of this function
     *
     *  I rewrote it to use getaddrinfo() again, and it works, so there you go.
     *
     *  libdill sucks, sometimes.
     * */
    int status = getaddrinfo(path, port_str, &hints, &result);
    if(status != 0)
    {
        log_fatal("Failed to fetch address information");
        perror("getaddrinfo");
        return -1;
    }

    // Force that the result is an IPV4 address
    // so the ipaddr struct works nicely.
    struct addrinfo *rp = result;
    ((struct sockaddr*)rp)->sa_family = AF_INET;

    // Since libdill ipaddr struct is just an array
    // of 32 chars, we can just copy the entire addrinfo struct
    // to the ipaddr struct.
    memcpy(res, rp->ai_addr, sizeof(struct sockaddr));

    return 0;
}
